const Clients = require('./clients');
const WebSocket = require("ws");

const clients = new Clients();

const wss = new WebSocket.Server({ port: 8101 });

let members = [];
const MAX_CAPACITY = 6;
let thumbnails = {};

const addMember = (data, client) => {
  const tokens = data.split("::::");
  const type = tokens[0];
  console.log(`Command ${type}`);
  if (type === "Members") {
    const { name, thumbnail } = JSON.parse(tokens[1]);
    if (!members.includes(name)) {
      members.push(name);
    }
    thumbnails[name] = thumbnail;
    clients.saveClient(name, client);

    let newThumbnail = {}
    newThumbnail[name] = thumbnail;

    // Create a response
    let response = {
      members: members,
      thumbnails: newThumbnail,
      message: name + " added successfully",
      isFull: members.length === MAX_CAPACITY,
      type: type,
    };

    return response;
  }
  // User wants to clear the session 
  else if (type === "Clear") {
    members = []
    thumbnails = {};

    // Create a response
    let response = {
      members: members,
      thumbnails: thumbnails,
      message: "Session successfully cleared",
      isFull: false,
      type: type,
    };

    return response;
  }
  return null;
};

/**
 * 
 * If we notice that the members in this has reached 
 * the maximum capacity, we clear everything in this session
 * and start over. Sockets connecting the client will be terminated 
 * if the terminateClient flag is set to true.  
 * 
 * MAX_CAPACITY a global variable 
 */
const restartSession = () => {
  if (members.length >= MAX_CAPACITY) {
    console.log("Reached capacity");
    members = []
    thumbnails = {};
    notifyAllMaxRoom();
    closeAllClients();
  }
}

/**
 * Close sockets for all the clients that are connected
 */
const closeAllClients = () => {
  Object.entries(clients.clientList).forEach(([name, clientWS]) => {
    clientWS.close();
  })
}

/**
 * Notify all that room capacity has reached maximum
 */
const notifyAllMaxRoom = () => {
  let response = {
    members: members,
    thumbnails: thumbnails,
    message: "Room filled",
    isFull: true,
    type: "fin",
  };
  Object.entries(clients.clientList).forEach(([name, clientWS]) => {
    clientWS.send(response);
  })
}

wss.on("connection", function connection(client) {
  client.on("message", function incoming(data) {
    console.log("Received Data: %s", new Date().toString());
    // console.log(data);
    let response = addMember(data, client);
    Object.entries(clients.clientList).forEach(([name, clientWS]) => {
      console.log(`Sending data to: ${name}`);
      if (clientWS === client) {
        console.log("All thumbnails");
        response["thumbnails"] = thumbnails;
      }

      if (clientWS.readyState === WebSocket.OPEN) {
        console.log(`Sending Response to ${name}`);
        // console.log(response);
        clientWS.send(JSON.stringify(response));
      }
    });
    restartSession();
  });
});
