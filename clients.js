/**
 * A class for creating new clients, and ensuring that
 * clients' sockets don't keep handshaking again 
 * with clients who have already completed the handshake.
 */

module.exports = class Clients {
    constructor() {
        this.clientList = {};
        this.saveClient = this.saveClient.bind(this);
    }
    saveClient(name, client) {
        this.clientList[name] = client;
    }
}
