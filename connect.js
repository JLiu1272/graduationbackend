let express = require("express");
let app = express();
let mysql = require("mysql");
let bodyParser = require("body-parser");
var cors = require("cors");
app.use(cors());

let connection = mysql.createConnection({
  host: "glados.ergonomic.cricket",
  user: "jenniferMac",
  password: "CoffeeBeans1919^^;",
  database: "grad_room"
});

let members = ["jennifer"];

connection.connect(function(err) {
  if (err) throw err;
  console.log("You are now connected with mysql database...");
});

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true
  })
);

var server = app.listen(3001, "localhost", function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log("Example app listening at http://%s:%s", host, port);
});

//rest api to get a single customer data
app.get("/customer", function(req, res) {
  res.end("Testing");
});

/*
 * Adds the person into the room
 */
app.post("/addMember", function(req, res) {
  let params = req.body;
  let name = params.name;
  params.status = 1;
  // console.log("Name: " + params.name);
  // console.log("Thumbnail: " + params.thumbnail);
  // console.log("Status: " + params.status);
  connection.query("INSERT INTO grad_room SET ?", params, function(
    error,
    results,
    fields
  ) {
    if (error) {
      return res.status(400).send("Error connecting to database.");
    }
    members.push(name);
    return res.status(200).send(name + " added successfully");
  });
});

/**
 * Gets all the members who are in the room
 */
//rest api to get a single customer data
app.get("/getMembers", function(req, res) {
  data = {
    members: members
  };
  return res.status(200).send(JSON.stringify(data));
});

/**
 * Get thumbail for a particular user
 */
app.post("/getThumbnail", function(req, res) {
  let params = req.body;
  connection.query("SELECT thumbnail FROM grad_room WHERE ?", params, function(
    error,
    results,
    fields
  ) {
    if (error) {
      return res.status(400).send(error);
    }
    return res.status(200).send(results);
  });
});
